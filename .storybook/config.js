import { configure } from "@storybook/react";
import "../src/styles/index.css";

configure(require.context("../src/stories", true, /Stories\.bs\.js$/), module);
