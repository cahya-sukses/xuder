open BsStorybook.Story;

storiesOf("My First Reason Story", [%bs.raw "module"])
->add("first chapter", () =>
    <span> "Hello bs-storybook!"->React.string </span>
  )
->add("second chapter", () =>
    <span> "Hello again bs-storybook!"->React.string </span>
  );