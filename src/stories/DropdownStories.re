open BsStorybook.Story;

storiesOf("Dropdown Stories", [%bs.raw "module"])
->add("Dropdown Input", () => {
    let values = [|("a", "123"), ("b", "456"), ("c", "789")|];
    <Dropdown values />;
  });