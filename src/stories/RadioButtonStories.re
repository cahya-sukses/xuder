open BsStorybook.Story;

storiesOf("Radio Button Stories", [%bs.raw "module"])
->add("Radio Button", () => {
    let (value, setValue) = React.useState(() => "");
    let onChange = event => setValue(ReactEvent.Form.target(event)##value);

    <div>
      <RadioButton
        checked={value == "kanan"} value="kanan" name="coba" onChange>
        "Kanan"->React.string
      </RadioButton>
      <RadioButton checked={value == "kiri"} value="kiri" name="coba" onChange>
        "Kiri"->React.string
      </RadioButton>
    </div>;
  });