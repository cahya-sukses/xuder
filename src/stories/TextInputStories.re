open BsStorybook.Story;

storiesOf("Text Input Stories", [%bs.raw "module"])
->add("Text Input", () => {
    let (value, setValue) = React.useState(() => "");
    let onChange = event => setValue(ReactEvent.Form.target(event)##value);
    <TextInput value onChange name="abcd" placeholder="abcd" />;
  });