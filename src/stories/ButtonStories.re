open BsStorybook.Story;

storiesOf("Button Stories", [%bs.raw "module"])
->add("Button", () =>
    <Button>"Ini button"->React.string</Button>
  )