[@bs.module "react-typed"] [@react.component]
external make:
  (
    ~strings: array(string),
    ~typeSpeed: int,
    ~backSpeed: int,
    ~loop: bool,
    ~showCursor: bool
  ) =>
  React.element =
  "default";