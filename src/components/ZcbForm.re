open Events;

let currencies = [|
  ("USD", "USD"),
  ("GBP", "GBP"),
  ("EUR", "EUR"),
  ("ZAR", "ZAR"),
  ("KYD", "KYD"),
  ("CHF", "CHF"),
|];

let stringToCurrency = str =>
  Contract.(
    switch (str) {
    | "USD" => USD
    | "GBP" => GBP
    | "EUR" => EUR
    | "ZAR" => ZAR
    | "KYD" => KYD
    | "CHF" => CHF
    | _ => USD
    }
  );

[@react.component]
let make = (~onDrawImage) => {
  let (time, setTime) = React.useReducer((_, v) => v, 1);
  let (currency, setCurrency) = React.useReducer((_, v) => v, Contract.USD);
  let (value, setValue) = React.useReducer((_, v) => v, "10.0");

  <>
    <label className="compose-label">
      <h5> "Horizon"->React.string </h5>
      <TextInput
        name="horizon"
        type_="number"
        onChange={e =>
          (
            switch (e->targetValue->Belt.Int.fromString) {
            | Some(i) => i
            | None => 0
            }
          )
          |> setTime
        }
        value={Belt.Int.toString(time)}
      />
    </label>
    <label className="compose-label">
      <h5> "Value"->React.string </h5>
      <TextInput name="Value" onChange={e => e->targetValue->setValue} value />
    </label>
    <label className="compose-label">
      <h5> "Currency"->React.string </h5>
      <Dropdown
        showFirst=true
        onChange={e => e->targetValue->stringToCurrency->setCurrency}
        name="currency"
        values=currencies
      />
    </label>
    <Button
      onClick={_e =>
        switch (value->Belt.Float.fromString) {
        | Some(value) => Contract.ZCB({time, currency, value})->onDrawImage
        | _ => ()
        }
      }>
      "Draw & Save"->React.string
    </Button>
  </>;
};
