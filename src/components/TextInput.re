Modules.require("./TextInput.css");

[@react.component]
let make =
    (~value="", ~type_="text", ~onChange=_e => (), ~name="", ~placeholder="") => {
  <input type_ className="xuder-input-text" value name placeholder onChange />;
};