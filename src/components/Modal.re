[@bs.module "react-modal"] [@react.component]
external make:
  (
    ~isOpen: bool,
    ~onAfterOpen: unit => unit,
    ~onRequestClose: unit => unit,
    ~style: string,
    ~contentLabel: string,
    ~children: React.element
  ) =>
  React.element =
  "default";

[@bs.module "react-modal"]
external setAppElement: string => unit = "setAppElement";