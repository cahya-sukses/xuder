[@react.component]
let make = (~route, ~children) => {
  <a
    href=route
    onClick={e => {
      ReactEvent.Mouse.preventDefault(e);
      ReasonReactRouter.push(route);
    }}>
    children
  </a>;
};