Modules.require("./NavBar.css");

type navbarDisplay = {
  route: string,
  displayStr: string,
};

let navValue = [|
  {route: "/", displayStr: "Home"},
  {route: "/about", displayStr: "About"},
  {route: "/compose", displayStr: "Compose"},
|];

[@react.component]
let make = () => {
  <nav className="xuder-navbar">
    <ul>
      {ReasonReact.array(
         Array.map(
           value =>
             <li>
               <Link route={value.route}>
                 value.displayStr->React.string
               </Link>
             </li>,
           navValue,
         ),
       )}
    </ul>
  </nav>;
};