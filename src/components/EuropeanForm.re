open Events;

[@react.component]
let make = (~onDrawImage) => {
  let state = XuderContext.useState();
  let (time, setTime) = React.useReducer((_, v) => v, 1);
  let (contract, setContract) = React.useReducer((_, v) => v, None);

  <>
    <label className="compose-label">
      <h5> "Horizon"->React.string </h5>
      <TextInput
        type_="number"
        value={time->Belt.Int.toString}
        onChange={e =>
          (
            switch (e->targetValue->Belt.Int.fromString) {
            | Some(i) => i
            | None => 0
            }
          )
          |> setTime
        }
        name="horizon"
      />
    </label>
    <label className="compose-label">
      <h5> "Underlying Contract"->React.string </h5>
      <Dropdown
        name="contracts"
        onChange={e =>
          List.find(c => fst(c) == e->targetValue, state.contracts)
          ->snd
          ->Some
          ->setContract
        }
        values={Array.map(
          value => (fst(value), fst(value)),
          Array.of_list(state.contracts),
        )}
      />
    </label>
    <Button
      onClick={_e =>
        switch (contract) {
        | Some(c) => Contract.(European({time, contract: c}))->onDrawImage
        | None => ()
        }
      }>
      "Draw & Save"->React.string
    </Button>
  </>;
};