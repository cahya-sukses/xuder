open Events;

[@react.component]
let make = (~onDrawImage) => {
  let state = XuderContext.useState();

  let (contract1, setContract1) = React.useReducer((_, v) => v, None);
  let (contract2, setContract2) = React.useReducer((_, v) => v, None);

  <>
    <label className="compose-label">
      <h5> "Contracts"->React.string </h5>
      <Dropdown
        name="contract1"
        values={Array.map(
          value => (fst(value), fst(value)),
          Array.of_list(state.contracts),
        )}
        onChange={e =>
          List.find(c => fst(c) == e->targetValue, state.contracts)
          ->snd
          ->Some
          ->setContract1
        }
      />
      <Dropdown
        name="contract2"
        values={Array.map(
          value => (fst(value), fst(value)),
          Array.of_list(state.contracts),
        )}
        onChange={e =>
          List.find(c => fst(c) == e->targetValue, state.contracts)
          ->snd
          ->Some
          ->setContract2
        }
      />
    </label>
    <Button
      onClick={_e =>
        switch (contract1) {
        | Some(c1) =>
          switch (contract2) {
          | Some(c2) =>
            Contract.(AndContracts({contracts: [c1, c2]}))->onDrawImage
          | None => ()
          }
        | None => ()
        }
      }>
      "Draw & Save"->React.string
    </Button>
  </>;
};