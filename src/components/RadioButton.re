Modules.require("./RadioButton.css");

[@react.component]
let make =
    (~value="", ~onChange=_e => (), ~name="", ~checked=false, ~children) => {
  <>
    <input
      type_="radio"
      className="xuder-radio-button"
      value
      name
      checked
      onChange
      id=value
    />
    <label htmlFor=value> children </label>
  </>;
};