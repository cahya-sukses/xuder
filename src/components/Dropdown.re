Modules.require("./Dropdown.css");

[@react.component]
let make = (~values, ~showFirst=false, ~onChange=_e => (), ~name="") => {
  <select type_="text" className="xuder-dropdown" name onChange>
    {showFirst
       ? React.null
       : <option disabled=true selected=true value="---">
           "---"->React.string
         </option>}
    {ReasonReact.array(
       Array.map(
         value =>
           <option value={fst(value)} key={fst(value)}> {snd(value)->React.string} </option>,
         values,
       ),
     )}
  </select>;
};
