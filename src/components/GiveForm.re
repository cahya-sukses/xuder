open Events;

[@react.component]
let make = (~onSave) => {
  let state = XuderContext.useState();
  let (contract, setContract) = React.useReducer((_, v) => v, None);

  <>
    <label className="compose-label">
      <h5> "Contract"->React.string </h5>
      <Dropdown
        name="contracts"
        values={Array.map(
          value => (fst(value), fst(value)),
          Array.of_list(state.contracts),
        )}
        onChange={e =>
          List.find(c => fst(c) == e->targetValue, state.contracts)
          ->snd
          ->Some
          ->setContract
        }
      />
    </label>
    <Button
      onClick={_e =>
        switch (contract) {
        | Some(c) => Contract.(GiveContract({contract: c}))->onSave
        | None => ()
        }
      }
    > "Save"->React.string </Button>
  </>;
};
