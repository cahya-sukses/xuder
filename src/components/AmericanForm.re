open Events;

[@react.component]
let make = (~onDrawImage) => {
  let state = XuderContext.useState();

  let (timeBefore, setTimeBefore) = React.useReducer((_, v) => v, 1);
  let (timeAfter, setTimeAfter) = React.useReducer((_, v) => v, 2);
  let (contract, setContract) = React.useReducer((_, v) => v, None);

  <>
    <label className="compose-label">
      <h5> "Time Before"->React.string </h5>
      <TextInput
        type_="number"
        value={timeBefore->Belt.Int.toString}
        onChange={e =>
          (
            switch (e->targetValue->Belt.Int.fromString) {
            | Some(i) => i
            | None => 0
            }
          )
          |> setTimeBefore
        }
        name="timebefore"
      />
    </label>
    <label className="compose-label">
      <h5> "Time After"->React.string </h5>
      <TextInput
        name="timeafter"
        type_="number"
        value={timeAfter->Belt.Int.toString}
        onChange={e =>
          (
            switch (e->targetValue->Belt.Int.fromString) {
            | Some(i) => i
            | None => 0
            }
          )
          |> setTimeAfter
        }
      />
    </label>
    <label className="compose-label">
      <h5> "Contract"->React.string </h5>
      <Dropdown
        name="contract"
        onChange={e =>
          List.find(c => fst(c) == e->targetValue, state.contracts)
          ->snd
          ->Some
          ->setContract
        }
        values={Array.map(
          value => (fst(value), fst(value)),
          Array.of_list(state.contracts),
        )}
      />
    </label>
    <Button
      onClick={_e =>
        switch (contract) {
        | Some(c) =>
          Contract.(American({timeBefore, timeAfter, contract: c}))
          ->onDrawImage
        | None => ()
        }
      }>
      "Draw & Save"->React.string
    </Button>
  </>;
};