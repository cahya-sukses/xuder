type contractType =
  | ZCB
  | European
  | American
  | GiveContract
  | AndContracts
  | OrContracts
  | NoType;

type state = {
  contractCount: int,
  image: option((string, string)),
  contractType,
  contracts: list((string, Contract.contract)),
};

let initialState = {
  contractCount: 1,
  contractType: NoType,
  contracts: [],
  image: None,
};

type action =
  | AddContract(string, Contract.contract)
  | ChangeType(contractType)
  | ClearImage
  | SetImage(string, string);

let reducer = (state, action) =>
  switch (action) {
  | AddContract(name, contract) => {
      ...state,
      contracts: [
        (
          "#" ++ Belt.Int.toString(state.contractCount) ++ " " ++ name,
          contract,
        ),
        ...state.contracts,
      ],
      contractCount: state.contractCount + 1,
    }
  | ChangeType(contractType) => {...state, contractType}
  | ClearImage => {...state, image: None}
  | SetImage(name, url) => {...state, image: Some((name, url))}
  };

module StateProvider = {
  let stateContext = React.createContext(initialState);

  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(stateContext);
};

module DispatchProvider = {
  let dispatchContext = React.createContext(_action => ());

  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(dispatchContext);
};

[@react.component]
let make = (~children) => {
  let (state, dispatch) = React.useReducer(reducer, initialState);

  <StateProvider value=state>
    <DispatchProvider value=dispatch> children </DispatchProvider>
  </StateProvider>;
};

let useDispatch = () => React.useContext(DispatchProvider.dispatchContext);
let useState = () => React.useContext(StateProvider.stateContext);