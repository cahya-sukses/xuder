[@react.component]
let make = () => {
  let url = ReasonReactRouter.useUrl();
  <XuderContext>
    {switch (url.path) {
     | [] => <HomePage />
     | ["counter"] => <CounterPage />
     | ["compose"] => <Compose />
     | ["about"] => <AboutPage />
     | _ => <NotFoundPage />
     }}
    <NavBar />
  </XuderContext>;
};