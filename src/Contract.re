type currency =
  | USD
  | GBP
  | EUR
  | ZAR
  | KYD
  | CHF;

type contract =
  | ZCB({
      time: int,
      value: float,
      currency,
    })
  | European({
      time: int,
      contract,
    })
  | American({
      timeAfter: int,
      timeBefore: int,
      contract,
    })
  | GiveContract({contract})
  | AndContracts({contracts: list(contract)})
  | OrContracts({contracts: list(contract)});

module Encode = {
  let currency = c =>
    Json.Encode.string(
      switch (c) {
      | USD => "USD"
      | GBP => "GBP"
      | EUR => "EUR"
      | ZAR => "ZAR"
      | KYD => "KYD"
      | CHF => "CHF"
      },
    );

  let rec contract = contractRecord =>
    Json.Encode.(
      switch (contractRecord) {
      | ZCB(fields) =>
        object_([
          ("tag", string("ZCB")),
          ("time", int(fields.time)),
          ("value", Json.Encode.float(fields.value)),
          ("currency", currency(fields.currency)),
        ])
      | European(fields) =>
        object_([
          ("tag", string("European")),
          ("time", int(fields.time)),
          ("contract", contract(fields.contract)),
        ])
      | American(fields) =>
        object_([
          ("tag", string("American")),
          ("timeAfter", int(fields.timeAfter)),
          ("timeBefore", int(fields.timeBefore)),
          ("contract", contract(fields.contract)),
        ])
      | GiveContract(fields) =>
        object_([
          ("tag", string("GiveContract")),
          ("contract", contract(fields.contract)),
        ])
      | AndContracts(fields) =>
        object_([
          ("tag", string("AndContracts")),
          ("contracts", contracts(fields.contracts)),
        ])
      | OrContracts(fields) =>
        object_([
          ("tag", string("OrContracts")),
          ("contracts", contracts(fields.contracts)),
        ])
      }
    )
  and contracts = cs => Json.Encode.list(contract, cs);
};