let toPayload = contract => {
  Json.Encode.(
    object_([
      (
        "exEval",
        object_([
          ("tag", string("EvalContract")),
          ("contents", Contract.Encode.contract(contract)),
        ]),
      ),
      ("imageType", string("svg")),
    ])
  );
};

module Decode = {
  let image = response => Json.Decode.(field("imgUrl", string, response));
};

let baseUrl = "https://danom.cahyanugraha12.me";
// let baseUrl = "http://localhost:3001";

let drawImage = contract =>
  Js.Promise.(
    Fetch.fetchWithInit(
      baseUrl ++ "/contract-ex",
      Fetch.RequestInit.make(
        ~method_=Post,
        ~body=contract->toPayload->Js.Json.stringify->Fetch.BodyInit.make,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => baseUrl ++ Decode.image(json) |> resolve)
  );