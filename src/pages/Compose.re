open Events;

Modules.require("./Compose.css");

let contractType = [|
  ("zcb", "Zero-coupon bond"),
  ("european", "European Options"),
  ("american", "American Options"),
  ("give", "Give Contract"),
  ("and", "And Contracts"),
  ("or", "Or Contracts"),
|];

Modal.setAppElement("#root");

[@react.component]
let make = () => {
  let dispatch = XuderContext.useDispatch();
  let state = XuderContext.useState();
  let (name, setName) = React.useReducer((_, v) => v, "My Contract");

  let changeContractType = event => {
    dispatch(
      ChangeType(
        switch (ReactEvent.Form.target(event)##value) {
        | "zcb" => ZCB
        | "european" => European
        | "american" => American
        | "give" => GiveContract
        | "and" => AndContracts
        | "or" => OrContracts
        | _ => NoType
        },
      ),
    );
  };

  let onSave = contract => {
    dispatch(AddContract(name, contract));
  };

  let onDrawImage = contract => {
    dispatch(AddContract(name, contract));
    Js.Promise.(
      Api.drawImage(contract)
      |> then_(imageUrl => dispatch(SetImage(name, imageUrl)) |> resolve)
    )
    |> ignore;
  };

  <div className="single-page compose-page">
    <div className="content">
      <h2> "Compose Contract"->React.string </h2>
      <Modal
        isOpen={Belt.Option.isSome(state.image)}
        onAfterOpen={() => ()}
        onRequestClose={() => ()}
        style=""
        contentLabel="Contract Image">
        {switch (state.image) {
         | Some((name, imageUrl)) =>
            <div>
              <div className="modal-header">
                <h3 className="modal-title"> name->React.string </h3>
                <Button onClick={_e => dispatch(ClearImage)}>
                  "Close"->React.string
                </Button>
              </div>
              <img className="diagram" src=imageUrl />
            </div>
         | None => React.null
         }}
      </Modal>
      <form
        onSubmit={e => ReactEvent.Form.preventDefault(e)}
        className="compose-form">
        <label className="compose-label">
          <h5> "Contract Name"->React.string </h5>
          <TextInput
            name="Value"
            onChange={e => e->targetValue->setName}
            value=name
          />
        </label>
        <label className="compose-label">
          <h5> "Contract Type"->React.string </h5>
          <Dropdown
            name="contract"
            values=contractType
            onChange=changeContractType
          />
        </label>
        {switch (state.contractType) {
         | ZCB => <ZcbForm onDrawImage />
         | European => <EuropeanForm onDrawImage />
         | American => <AmericanForm onDrawImage />
         | GiveContract => <GiveForm onSave />
         | AndContracts => <AndForm onDrawImage />
         | OrContracts => <OrForm onDrawImage />
         | NoType => React.null
         }}
      </form>
    </div>
  </div>;
};
