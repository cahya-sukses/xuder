Modules.require("./NotFoundPage.css");

[@react.component]
let make = () => {
  <div className="not-found-page single-page">
    <div className="content">
      <h1> "Not Found"->React.string </h1>
      <h3> "But you can find your future here"->React.string </h3>
      <Link route="/"> <Button> "Back to home"->React.string </Button> </Link>
    </div>
  </div>;
};