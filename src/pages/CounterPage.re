type action =
  | Reset
  | Increment;

let initialState = 0;

let reducer = (count, action) =>
  switch (action) {
  | Reset => initialState
  | Increment => count + 1
  };

[@react.component]
let make = () => {
  let (count, dispatch) = React.useReducer(reducer, initialState);

  <div>
    <h1> "Hello from Reason"->React.string </h1>
    <div> "Counter"->React.string </div>
    <div> {count->Belt.Int.toString->React.string} </div>
    <button onClick={_e => dispatch(Reset)}> "Reset"->React.string </button>
    <button onClick={_e => dispatch(Increment)}>
      "Increment"->React.string
    </button>
  </div>;
};