Modules.require("./AboutPage.css");
let cahya = Modules.require("../assets/cahya.png");

[@react.component]
let make = () => {
  <div className="about-page single-page">
    <div className="content">
      <h1> "About"->React.string </h1>
      <h3> "Omonib by PT Cahaya Sukses Sejahtera"->React.string </h3>
      <img src=cahya alt="Cahya" />
      <h4> "Cahya \"Sukses\" Nugraha"->React.string </h4>
      <h5> "CEO, CTO, C*O, & Founder"->React.string </h5>
      <p>
        "\"Saya mulai membangun Omonib saat saya bisa membeli 1000 kontrak dalam
        sehari tanpa keluar rumah. Kini giliran anda yang melakukannya.\""
        ->React.string
      </p>
    </div>
  </div>;
};