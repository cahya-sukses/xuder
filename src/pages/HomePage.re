Modules.require("./HomePage.css");

[@react.component]
let make = () => {
  <div className="landing-page single-page">
    <div className="content">
      <h1> "Omonib"->React.string </h1>
      <h3>
        <Typed
          strings=[|"Compose", "Create", "Plan", "Evaluate"|]
          typeSpeed=50
          backSpeed=40
          loop=true
          showCursor=false
        />
        " your contract"->React.string
      </h3>
      <Link route="/compose">
        <Button> "Compose now"->React.string </Button>
      </Link>
    </div>
  </div>;
};