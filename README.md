# Xuder

## Live Version

[https://omonib.netlify.com/](https://omonib.netlify.com/)

## Prototype

[https://www.figma.com/file/ZUcZ4Q9vTi8II59ublHSZv/Xuder](https://www.figma.com/file/ZUcZ4Q9vTi8II59ublHSZv/Xuder)

## How To Run

- Clone this repo
- Install [Yarn](https://yarnpkg.com/en/docs/install) (Recommended) or
  [npm](https://www.npmjs.com/get-npm)
- In root folder, install dependencies.

  Using Yarn.

  ```bash
  yarn
  ```

  Using npm.

  ```bash
  npm install
  ```

- Run reasonml transpiler.

  Using Yarn.

  ```bash
  yarn re:watch
  ```

  Using npm.

  ```bash
  npm run re:watch
  ```

- Run react app in another terminal.

  Using Yarn.

  ```bash
  yarn start
  ```

  Using npm.

  ```bash
  npm start
  ```

- Open `http://localhost:3000` in browser.
